console.log("Hello World!");

let trainer = {
    name: "Kiawe",
    age: 17,
    pokemon: ["Snorlax","Mewtwo","Charizard"],
    friends: {
        sinnoh: ["Alder","Cynthia","Blue"],
        galar: ["Drake","Giovanni","Lance"]
    }
};

trainer.talk = function(name){
    console.log(`${trainer.pokemon[2]}! I choose you!`);
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of squre bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method:");
trainer.talk();

function Pokemon(name,level){
    // properties
    this.name = name;
    this.level = level;
    this.health = 2* level;
    this.attack = level;
    // methods
    this.tackle = function(target){
        if(this.attack !== 0){
            console.log(`${this.name} tackled ${target.name}!`);
            if(target.health > this.attack){
                console.log(`${target.name}'s health is now reduced to ${target.health = target.health - this.attack}.`);
            }
            else{
                console.log(`${target.name}'s health is now 0.`);
                target.faint();
                target.health = 0;
                target.attack = 0;
            }
        }
        else{
            console.log(`${this.name} is already fainted. Can't tackle anymore.`);
        }
    };
    this.faint = function(){
        console.log(this.name +" fainted.");
    };
}

let snorlax = new Pokemon("Snorlax",18);
console.log(snorlax);
let gyarados = new Pokemon("Gyarados",30);
console.log(gyarados);
let onix = new Pokemon("Onix",25);
console.log(onix);
console.log("---------");
console.log(snorlax);
snorlax.tackle(gyarados);
console.log(gyarados);
gyarados.tackle(onix);
console.log(onix);
onix.tackle(snorlax);
console.log(snorlax);
snorlax.tackle(gyarados);
console.log(gyarados);
gyarados.tackle(onix);
console.log(onix);
onix.tackle(snorlax);
console.log(snorlax);
snorlax.tackle(gyarados);
console.log(gyarados);
gyarados.tackle(onix);
console.log(onix);
onix.tackle(snorlax);

