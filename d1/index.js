console.log("Hello World!");
/**
 * SECTION -Objects
 *  objects are data types that are used to represent real world objects
 *  collection of realted data and/or functionalities
 *  in Javascript , most features like strings and arrays are considered to be objects
 *  their difference in javascript objects is that the JS object has "key: value" pair
 *  keys are also referred to as properties while the value is the figure on the right side of the colon
 * SYNTAX:
 *  let/const objectName = {
 *      keyA: valueA,
 *      keyB: valueB
 * }
 */
let cellphone ={
    name:"Nokia 3210",
    manufacturedDate: 1999
}
console.log("Result from creating objects using intializers");
console.log(cellphone);
console.log(typeof cellphone);

// let car = {
//     name:"Nissan Terra",
//     yearReleased:"2022"
// };

// console.log("Car");
// console.log(car);
// console.log(typeof car);

// creating objects using constructor function
/**
 * this is useful for creating multiple instances/copies of an object
 * SYNTAX: function ObjectName(keyA,keyB){
 *      this.keyA = keyA;
 *      this.keyB = keyB
 * }
 */
function Laptop(name, manufacturedDate) {
        this.name = name;
        this.manufacturedDate = manufacturedDate;
}
/**
 *  new keyword creates an instance of an object
 *  objects and instances are often interchanged because object literals and instances are distinct/unique
 */
let laptop = new Laptop("Dell", 2012);
console.log("Result from creating objects using intializers");
console.log(laptop);
console.log(typeof laptop);

let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using intializers");
console.log(laptop2);
console.log(typeof laptop2);

// create empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using intializers");
console.log(myComputer);
console.log(typeof myComputer);

// Accessing objects
/**
 * using dot notation
 */
console.log("Result from dot notation: "+ laptop.name);
// Array of objects

let laptops = [laptop,laptop2];
console.log(laptops[1].name);
// access laptops array - laptop - manufacturedDate
console.log(laptops[0].manufacturedDate);

/**
 * initializng, adding, deleting and reasigning object properties
 * like any ther vairables in javascript, objects may have their properties initialized or added after the object was created/declared
 */

let car = {};

car.name = "Honda Vios";
car.manufacturedDate = 2022;
console.log(car);

car["manufactured date"] = 2019;
console.log(car);

// deleting of properties
delete car["manufactured date"];
console.log(car);

car.name = "Dodge Charger R/T";
console.log(car);

// Object methods
/**
 * an object method is a function that is set by a dev to be the value of one of the properties
 * they are also functions and one of the differences they have us that methods are functions related to a specific object
 */
let person = {
    name: "John",
    talk: function(){
        console.log("Hello! My name is "+ this.name);
    }
}
person.talk();
person.walk = function(){
    // console.log(`${this.name} walked 25 steps`);
    console.log(`${this.name} walked 25 steps`);
}
person.walk();
//template literals backticks + ${}

let friend = {
    firstName:"Joe",
    lastName:"Smith",
    address:{
        city: "Austin",
        state:"Texas"
    },
    emails:['joe@mail.com',"johnHandsome@mail.com"],
    introduce: function(){
        console.log(`Hello! My name is ${this.firstName} ${this.lastName}.`);
    }
};
friend.introduce();
console.log(friend);

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
    },
    faint: function(){
        console.log("Pokemon Faineted");
    }
};
console.log(myPokemon);

function Pokemon(name,level){
    // properties
    this.name = name;
    this.level = level;
    this.health = 2* level;
    this.attack = level;
    // methods
    this.tackle = function(target){
        console.log(this.name + " tackled "+ target.name);
        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
    };
    this.faint = function(){
        console.log(this.name +" fainted.");
    };
}

let snorlax = new Pokemon("Snorlax", 17);
let mewtwo = new Pokemon("Mewtwo",12);

snorlax.tackle(mewtwo);
